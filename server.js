const displayPlace = document.querySelector("#content .container");
function display(data, pattern) {
  // const displayPlace=document.querySelector("#content .container")
  const fragment = document.createDocumentFragment();
  if (pattern) {
    pattern = pattern.toUpperCase();
    displayPlace.innerText = "";
  }
  data["hits"].forEach((item) => {
    const div1 = document.createElement("div");
    const div2 = document.createElement("div");

    const titleLink = document.createElement("a");
    const urlLink = document.createElement("a");
    const pointsLink = document.createElement("a");
    const authorLink = document.createElement("a");
    const createdLink = document.createElement("a");
    const commentsLink = document.createElement("a");

    titleLink.href = item.url;
    titleLink.innerText = item.title;
    titleLink.style.fontWeight = "bold";

    const upperCaseTitle = item.title.toUpperCase();
    if (pattern && upperCaseTitle.search(pattern) != -1) {
      titleLink.style.backgroundColor = "orange";
    }

    urlLink.href = item.url;
    urlLink.innerText = `(${item.url})`;

    urlString = JSON.stringify(item.url).toUpperCase();
    if (pattern && urlString.search(pattern) != -1) {
      urlLink.style.backgroundColor = "orange";
    }

    pointsLink.href = item.url;
    pointsLink.innerText = `${item.points} points`;

    authorLink.href = item.url;
    authorLink.innerText = item.author;

    const upperCaseAuthor = item.author.toUpperCase();
    if (pattern && upperCaseAuthor.search(pattern) != -1) {
      authorLink.style.backgroundColor = "orange";
    }

    createdLink.href = item.url;
    createdLink.innerText = item.created_at;

    commentsLink.href = item.url;
    commentsLink.innerText = `${item.num_comments} comments`;

    div1.appendChild(titleLink);
    div1.appendChild(urlLink);
    div2.appendChild(pointsLink);
    div2.append("|");
    div2.appendChild(authorLink);
    div2.append("|");
    div2.appendChild(commentsLink);

    div2.style.marginBottom = "0.4rem";
    fragment.appendChild(div1);
    fragment.appendChild(div2);
  });
  displayPlace.appendChild(fragment);
}

const searchString = document.querySelector("#search_box");

function starterPage() {
  displayPlace.innerText = "";
  fetch("https://hn.algolia.com/api/v1/search?tags=front_page")
    .then((res) => res.json())
    .then((data) => {
      display(data);
    })
    .catch((err) => console.log(err));

  // const searchString=document.querySelector("#search_box")

  searchString.addEventListener("keypress", (event) => {
    const url = `https://hn.algolia.com/api/v1/search?query=${event.target.value}&tags=story`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        display(data, event.target.value);
      })
      .catch((err) => console.log(err));
  });
}
starterPage();

const sortBy = document.querySelector("#by");

sortBy.addEventListener("click", (event) => {
  if (event.target.value === "Date") {
    fetch(`https://hn.algolia.com/api/v1/search_by_date?tags=story`)
      .then((res) => res.json())
      .then((data) => {
        displayPlace.innerText = "";
        display(data);
      });
  } else {
    starterPage();
  }
});

const time = document.querySelector("#time_range");

time.addEventListener("click", () => {
  starterPage();
});

const type = document.querySelector("#to_search");

type.addEventListener("click", () => {
  starterPage();
});
